# THESIS MOVIES-HD
---
> Web application for the sale of films in digital format.

> For more information, check out the publication of the thesis on the university website -> [Bachelor thesis](http://hdl.handle.net/10553/57831)

## Demo
---
> You can watch the full demo on YouTube -> [https://youtu.be/LBra9hQ2zXU](https://youtu.be/LBra9hQ2zXU)

### Intro

![Thesis-MoviesHD-Intro](https://res.cloudinary.com/comparte/image/upload/v1637018852/thesis-movieshd-intro.gif)

### Login system

![Thesis-MoviesHD-Login](https://res.cloudinary.com/comparte/image/upload/v1637019455/thesis-movieshd-login.gif)

### Payment gateway

#### Card payment

![Thesis-MoviesHD-Card-Payment](https://res.cloudinary.com/comparte/image/upload/v1637020909/thesis-movieshd-card-payment.gif)

#### Paypal payment

![Thesis-MoviesHD-Paypal-Payment](https://res.cloudinary.com/comparte/image/upload/v1637020871/thesis-movieshd-paypal-payment.gif)

### Purchase confirmation emails

![Thesis-MoviesHD-Purchase-Confirmation](https://res.cloudinary.com/comparte/image/upload/v1637127831/thesis-movieshd-purchase-confirmation-emails.gif)

## Built with 🛠️
---
![Tech stack](https://res.cloudinary.com/comparte/image/upload/v1636960698/Tech_stack.png)

- [Spring Projects](https://spring.io/projects) - Spring Framework, Spring Boot, Spring Data, Spring Security.
- [AWS](https://aws.amazon.com/products/) - Amazon S3, Amazon CloudFront, Amazon RDS, Amazon Route 53, Amazon Certificate Manager.
- [Thymeleaf](https://www.thymeleaf.org/) - A modern server-side Java template engine for both web and standalone environments.
- [Bootstrap](https://getbootstrap.com/) - Quickly design and customize responsive mobile-first sites, the world’s most popular front-end open source toolkit.
- [jQuery](https://jquery.com/) - A fast, small, and feature-rich JavaScript library.
- [jQuery UI](https://jqueryui.com/) - A curated set of user interface interactions, effects, widgets, and themes built on top of the jQuery JavaScript Library.
- [Braintree](https://developer.paypal.com/braintree/docs/) - Resources and tools for developers to integrate Braintree's global payments platform.
- [Firebase Auth](https://firebase.google.com/docs/auth) - Provides backend services, easy-to-use SDKs, and ready-made UI libraries to authenticate users to your app.

> For more information, check out the publication of the thesis on the university website -> [Bachelor thesis](http://hdl.handle.net/10553/57831)

## Author: Sergio Lopez Diaz ✍️
---
You can find out more about me and my projects on:

- [![](https://img.shields.io/badge/Follow-blue?style=social&logo=linkedin&logoColor=blue&labelColor=blue&color=gray)](https://www.linkedin.com/in/sldiaz04us "Sergio Lopez Diaz")
- [![](https://img.shields.io/badge/Follow-blue?style=social&logo=github&color=gray)](https://github.com/sldiaz04us "Sergio Lopez Diaz")
- [![](https://img.shields.io/badge/Follow-blue?style=social&logo=twitter&logoColor=blue&labelColor=blue&color=gray)](https://twitter.com/sldiaz04us "Sergio Lopez Diaz")
