let localeLanguage = document.getElementById('locale-language').textContent;
let reCaptchaUrl = 'https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=' + localeLanguage;
document.getElementById('reCaptcha-api').src = reCaptchaUrl;

function onloadCallback() {
    grecaptcha.render('reCaptcha', {
        'sitekey': '6Lc6A4oUAAAAAOLYgSI0VnGYy1WOjLMjXabc-iK-',
        'theme': 'dark',
        'callback': 'successful'
    });
}

let reCaptcha = false;
document.getElementById('reCaptcha-form').addEventListener('submit', function (event) {
    if (this.checkValidity() === true && reCaptcha) {
        this.submit();
    } else {
        event.preventDefault();
        event.stopPropagation();
    }
});

function successful(response) {
    reCaptcha = true;
}