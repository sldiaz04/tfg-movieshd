$(document).ready(function () {
    //initialize swiper when document ready
    let carouselSwiper = new Swiper('.swiper-carousel', {
        // Optional parameters
        loop: true,
        autoplay: {
            disableOnInteraction: false,
        },
        // Responsive breakpoints
        breakpointsInverse: true,
        breakpoints: {
            // when window width is <= 320px
            320: {},
            // when window width is <= 480px
            480: {},
            // when window width is <= 640px
            640: {}
        },
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
        },
        centeredSlides: true,
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar'
        },
    });

    let moviesSwiper = new Swiper('.swiper-movies', {
        slidesPerView: 2,
        /*slidesPerColumn: 2,*/
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
            clickable: true,
        },
        breakpointsInverse: true,
        breakpoints: {
            // when window width is => 340px
            340: {
                slidesPerView: 3,
            },
            // when window width is => 530px
            530: {
                slidesPerView: 4,
            },
            // when window width is => 990px
            990: {
                slidesPerView: 6,
                spaceBetween: 20,
            },
            // when window width is => 1200px
            1200: {
                slidesPerView: 8,
            }
        }
    });
});
