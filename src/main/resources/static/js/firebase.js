// FirebaseUI config.
let uiConfig = {
    signInSuccessUrl: '/',
    signInFlow: 'popup',
    signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID
    ],
    callbacks: {
        signInSuccessWithAuthResult: function (authResult, redirectUrl) {
            document.getElementById('ajax-loader').style.display = 'block';
            sendIdTokenToServer();
        },
        signInFailure: function (error) {
            manageErrorSignIn();
        },
        uiShown: function () {
            // The widget is rendered.
            // Hide the loader.
            document.getElementById('firebaseui-loader').style.display = 'none';
        }
    },
    // tosUrl and privacyPolicyUrl accept either url string or a callback
    // function.
    // Terms of service url/callback.
    tosUrl: '/info/terms',
    // Privacy policy url/callback.
    privacyPolicyUrl: '/info/policy'
};

function sendIdTokenToServer() {
    firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function (idToken) {
        $.ajax({
            url: "/firebase/idToken",
            data: {
                idToken: idToken
            },
            success: function (response) {
                if (response === 'OK') {
                    window.location.reload();
                } else if (response === 'ERROR') {
                    manageErrorSignIn();
                }
            },
            error: function (jqXHR, statusCode, error) {
                manageErrorSignIn();
                console.log(statusCode);
                console.log(error);
            }
        });
    }).catch(function (error) {
        manageErrorSignIn();
        console.log(error);
    });
}

function manageErrorSignIn() {
    document.getElementById('ajax-loader').style.display = 'none';
    mdtoast('Error to Sign In', {
        type: 'error',
        interaction: true,
        actionText: 'OK'
    });
    firebase.auth().signOut();
}

let ui;

startFirebaseUI = function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (!user) {
            // The start method will wait until the DOM is loaded.
            ui.start('#firebaseui-auth-container', uiConfig);
        }
    }, function (error) {
        console.log(error);
    });
};

initFirebaseUI = function () {
    let localeLanguage = document.getElementById('locale-language').textContent;
    let firebaseUI = document.getElementById('firebase-ui');
    firebaseUI.type = 'text/javascript';
    firebaseUI.async = true;
    firebaseUI.onload = function () {
        // Initialize the FirebaseUI Widget using Firebase.
        ui = new firebaseui.auth.AuthUI(firebase.auth());
        startFirebaseUI();
    };
    firebaseUI.src = 'https://www.gstatic.com/firebasejs/ui/3.5.2/firebase-ui-auth__' + localeLanguage + '.js';
};
window.addEventListener('load', function () {
    initFirebaseUI();
});