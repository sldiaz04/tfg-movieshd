$(document).ready(function () {
    $("#closeModal").click(function () {
        $("#modalMovieTrailer").modal("hide");
    });
    $("#modalMovieTrailer").on('hidden.bs.modal', function () {
        stopVideoModal();
    });
});
function stopVideoModal() {
    let elemento = document.querySelector('#videoframe');
    let src = elemento.getAttribute('src');
    elemento.setAttribute('src', '');
    elemento.setAttribute('src', src);
}