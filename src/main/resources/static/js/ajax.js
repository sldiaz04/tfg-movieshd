// Cart movies
$('.cart-icon a').click(function (e) {
    e.preventDefault();
    let link = $(this);
    let id = link.attr('value');
    $.ajax({
        url: "/cart/add-movie",
        data: {
            id: id
        },
        success: function (response) {
            $('.cart-icon-amount').text(response.items);

            // For the movie cart selected
            link.addClass("not-active");
            $("i", link).addClass("text-warning");

            // For the rest of the movies in the different sections of the homepage
            $("a[href='#'][value=" + id + "]").addClass("not-active");
            $("i", "a[href='#'][value=" + id + "]").addClass("text-warning");

            mySnackbar(response);
            console.log(response);
        },
        error: function (jqXHR, statusCode, error) {
            console.log(statusCode);
            console.log(error);
        }
    })
});

$("button[id*='movie_']").click(function (e) {
    e.preventDefault();
    let id = $(this).attr('value');
    let line = 'line_' + id;
    let totalPrice = $("#payment-price");
    $.ajax({
        url: "/cart/remove-movie",
        data: {
            id: id
        },
        success: function (response) {
            if (parseInt(response.items) === 0) {
                $('#cart-detail').remove();
                $('#payment-detail').remove();
            }
            $('.cart-icon-amount').text(response.items);
            totalPrice.text((parseFloat(totalPrice.text()) - parseFloat(response.movie.price)).toFixed(2));
            $("#" + line).remove();
            console.log(response);
        },
        error: function (jqXHR, statusCode, error) {
            console.log(statusCode);
            console.log(error);
        }
    })
});

$('#empty-cart').click(function (e) {
    e.preventDefault();
    $.ajax({
        url: "/cart/empty",
        success: function (response) {
            $('#cart-detail').remove();
            $('#payment-detail').remove();
            $('.cart-icon-amount').text(response);
            console.log(response);
        },
        error: function (jqXHR, statusCode, error) {
            console.log(statusCode);
            console.log(error);
        }
    })
});

// Favorite movies
$('.favorite-icon a').click(function (e) {
    e.preventDefault();

    let link = $(this);
    link.css({display: "none"});
    link.next('.spinner-border').css({display: "block"});

    let id = link.attr('value');
    let url;

    if ($("i", link).hasClass("text-dark-red")) {
        url = '/favorite/remove-movie';
    } else {
        url = '/favorite/add-movie';
    }
    $.ajax({
        url: url,
        data: {
            id: id
        },
        success: function (response) {
            $('.favorite-icon-amount').text(response);
            link.next('.spinner-border').css({display: "none"});
            link.css({display: "block"});

            // For all the movies in the different sections of the homepage
            $("i", "a[href='/favorite/add-movie'][value=" + id + "]").toggleClass("text-dark-red");

            console.log(response);
        },
        error: function (jqXHR, statusCode, error) {
            console.log(statusCode);
            console.log(error);
        }
    })
});

function mySnackbar(response) {
    let img = "<img class='snackbar-image' src=" + response.movie.posterImageURL + " />";
    let div = $("<div class='snackbar' id=snackbar_" + response.movie.id + "></div>").append(img).append("<span>" + response.movie.title + "</span>");
    $('footer').append(div);
    let snackbar = $('#snackbar_' + response.movie.id);
    snackbar.toggleClass('show');
    setTimeout(function () {
        snackbar.remove();
    }, 3000);
}