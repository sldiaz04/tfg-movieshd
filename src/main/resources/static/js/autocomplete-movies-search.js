$(document).ready(function () {
    autocomplete('.search-movie-header');
    autocomplete('.search-movie-fullscreen');
});

function autocomplete(form) {
    $(form).autocomplete({
        minLength: 2, // The minimum number of characters a user must type before a search is performed
        open: function (event, ui) {
            $(".ui-autocomplete").css("width", $(this).width() + 24);
            enableScroll();
        },
        close: function (event, ui) {
            disableScroll();
        },
        focus: function (event, ui) {
            $(this).val(ui.item.title);
            return false;
        },
        select: function (event, ui) {
            if (window.location.href.includes('localhost')) {
                window.location.replace("http://localhost:8080/movie/" + ui.item.id);
            } else {
                window.location.replace("https://www.movies-hd.cf/movie/" + ui.item.id);
            }
        },
        source: function (request, response) {
            $.ajax({
                url: "/movie/",
                dataType: "json",
                data: {// for request param
                    term: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            title: item.title,
                            image: item.posterImageURL
                        };
                    }));
                }
            });
        }
    }).autocomplete('instance')._renderItem = function (ul, item) {
        return $('<li>')
            .append("<img class='poster-image' src=" + item.image + " alt=" + item.title + " />")
            .append("<a href='/movie/" + item.id + "'>" + item.title + "</a>")
            .appendTo(ul);
    };
}

function preventSearchEmptyField(formInputValue) {
    return ($(formInputValue).val().length !== 0);
}