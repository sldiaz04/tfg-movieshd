package es.ulpgc.tfg.movieshd.email;

import es.ulpgc.tfg.movieshd.email.model.Mail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;
import java.util.Map;

@Service
public class MailClientBuilder {

    private final Log logger = LogFactory.getLog(this.getClass());

    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    @Autowired
    public MailClientBuilder(JavaMailSender mailSender, TemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Async
    public void prepareAndSend(Mail mail, Map<String, Object> params, Locale locale) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(mail.getTo());
            messageHelper.setSubject(mail.getSubject());
            String content = build(mail.getEmailTemplate(), params, locale);
            messageHelper.setText(content, true);
        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            logger.error("Error in sending the email: " + e.getMessage());
        }
    }

    private String build(String template, Map<String, Object> params, Locale locale) {
        Context context = new Context();
        context.setLocale(locale);

        for (Map.Entry<String, Object> sets : params.entrySet()) {
            context.setVariable(sets.getKey(), sets.getValue());
        }

        return templateEngine.process(template, context);
    }

}
