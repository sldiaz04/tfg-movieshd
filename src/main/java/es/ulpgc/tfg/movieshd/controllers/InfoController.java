package es.ulpgc.tfg.movieshd.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Locale;

@Controller
@RequestMapping("info")
public class InfoController {

    private static final String INFO = "info/";

    @GetMapping("faqs")
    public String faqs(Locale locale) {
        return INFO + locale.getLanguage() + "/faqs";
    }

    @GetMapping("terms")
    public String terms(Locale locale) {
        return INFO + locale.getLanguage() + "/terms_and_conditions";
    }

    @GetMapping("policy")
    public String policy(Locale locale) {
        return INFO + locale.getLanguage() + "/data_policy";
    }

    @GetMapping("contact")
    public String contact(Locale locale) {
        return INFO + locale.getLanguage() + "/contact";
    }

    @GetMapping("about")
    public String about(Locale locale) {
        return INFO + locale.getLanguage() + "/about";
    }
}
