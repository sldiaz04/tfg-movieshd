package es.ulpgc.tfg.movieshd.controllers;

import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.services.IMovieService;
import es.ulpgc.tfg.movieshd.utils.MathTool;
import es.ulpgc.tfg.movieshd.utils.paginator.PaginationFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Locale;

@Controller
public class MovieController {

    private final IMovieService movieService;
    private MessageSource messageSource;

    private static final String REDIRECT = "redirect:/";
    private static final String MOVIES_SEARCHED = "movies/movies_searched";

    @Autowired
    public MovieController(IMovieService movieService, MessageSource messageSource) {
        this.movieService = movieService;
        this.messageSource = messageSource;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("carouselMovies", movieService.findFirst4ByOrderByTotalFavoritesDesc());
        model.addAttribute("mostWatchedMovies", movieService.findFirst10ByOrderByTotalViewsDesc());
        model.addAttribute("mostPurchasedMovies", movieService.findFirst10ByOrderByTotalPurchasesDesc());
        model.addAttribute("mostFavoriteMovies", movieService.findFirst10ByOrderByTotalFavoritesDesc());
        return "index";
    }

    @GetMapping(value = "/movie/{id}")
    public String movieDetail(@PathVariable("id") String id, Model model, RedirectAttributes flash, Locale locale) {
        if (!MathTool.isNumeric(id)) {
            flash.addFlashAttribute("error", messageSource.getMessage("text.movie.detail.error", null, locale));
            return REDIRECT;
        }
        Movie movie = movieService.findById(Long.parseLong(id));
        if (movie == null) {
            flash.addFlashAttribute("info", messageSource.getMessage("text.movie.detail.no.result", null, locale));
            return REDIRECT;
        }
        model.addAttribute("movie", movie);
        return "movies/movie_detail";
    }

    @GetMapping(value = "/movie/", produces = "application/json")
    public @ResponseBody
    List<Movie> searchMovie(@RequestParam(value = "term") String term) {
        return movieService.findByName(term);
    }

    @GetMapping(value = "/movies/searched/")
    public String searchMovieForm(@RequestParam(name = "page", defaultValue = "0") int page,
                                  @RequestParam(value = "term") String term,
                                  Model model, Device device, Locale locale) {
        Page<Movie> movies = movieService.findByName(term, PaginationFactory.createPageRequest(page));
        String message = String.format(messageSource.getMessage("text.movie.search.result", null, locale), term);
        PaginationFactory.addAttributesToModel(model, message, "/movies/searched/?term=" + term, movies, device);
        return MOVIES_SEARCHED;
    }

    @GetMapping(value = "/movies/genre/{name}")
    public String searchMovieByGenre(@RequestParam(name = "page", defaultValue = "0") int page,
                                     @PathVariable(value = "name") String genre,
                                     Model model, Device device, Locale locale) {
        String source = switchMovieGenre(genre);

        Page<Movie> movies = movieService.findMoviesByGenre(genre, PaginationFactory.createPageRequest(page));
        PaginationFactory.addAttributesToModel(model, messageSource.getMessage(source, null, locale), "/movies/genre/" + genre, movies, device);
        return MOVIES_SEARCHED;
    }

    @GetMapping(value = "/movies/language/{lang}")
    public String searchMovieByLanguage(@RequestParam(name = "page", defaultValue = "0") int page,
                                        @PathVariable(value = "lang") String language,
                                        Model model, Device device, Locale locale, RedirectAttributes flash) {
        String source = "";
        switch (language) {
            case "spanish":
                source = "text.movie.language.spanish";
                break;
            case "english":
                source = "text.movie.language.english";
                break;
            default:
                String message = String.format(messageSource.getMessage("text.movie.language.search.error", null, locale), language);
                flash.addFlashAttribute("info", message);
                return REDIRECT;
        }

        Page<Movie> movies = movieService.findByLanguage(language, PaginationFactory.createPageRequest(page));
        PaginationFactory.addAttributesToModel(model, messageSource.getMessage(source, null, locale), "/movies/language/" + language, movies, device);
        return MOVIES_SEARCHED;
    }

    @GetMapping(value = "/movies/favorites/all")
    public String searchAllFavoritesMoviesOrdered(@RequestParam(name = "page", defaultValue = "0") int page,
                                                  Model model, Device device, Locale locale) {
        Page<Movie> movies = movieService.findByOrderByTotalFavoritesDesc(PaginationFactory.createPageRequest(page));
        PaginationFactory.addAttributesToModel(model, messageSource.getMessage("text.movie.favorites.all", null, locale), "/movies/favorites/all", movies, device);
        return MOVIES_SEARCHED;
    }

    @GetMapping(value = "/movies/purchases/all")
    public String searchAllPurchasesMoviesOrdered(@RequestParam(name = "page", defaultValue = "0") int page,
                                                  Model model, Device device, Locale locale) {
        Page<Movie> movies = movieService.findByOrderByTotalPurchasesDesc(PaginationFactory.createPageRequest(page));
        PaginationFactory.addAttributesToModel(model, messageSource.getMessage("text.movie.purchased.all", null, locale), "/movies/purchases/all", movies, device);
        return MOVIES_SEARCHED;
    }

    @GetMapping(value = "/movies/views/all")
    public String searchAllViewsMoviesOrdered(@RequestParam(name = "page", defaultValue = "0") int page,
                                              Model model, Device device, Locale locale) {
        Page<Movie> movies = movieService.findByOrderByTotalViewsDesc(PaginationFactory.createPageRequest(page));
        PaginationFactory.addAttributesToModel(model, messageSource.getMessage("text.movie.viewed.all", null, locale), "/movies/views/all", movies, device);
        return MOVIES_SEARCHED;
    }

    private String switchMovieGenre(String genre) {
        switch (genre) {
            case "action":
                return "text.movie.genre.action";
            case "animation":
                return "text.movie.genre.animation";
            case "comedy":
                return "text.movie.genre.comedy";
            case "drama":
                return "text.movie.genre.drama";
            case "fantasy":
                return "text.movie.genre.fantasy";
            case "horror":
                return "text.movie.genre.horror";
            case "thriller":
                return "text.movie.genre.thriller";
            case "war":
                return "text.movie.genre.war";
            case "documentary":
                return "text.movie.genre.documentary";
            default:
                return "text.movie.genre.action";
        }
    }
}
