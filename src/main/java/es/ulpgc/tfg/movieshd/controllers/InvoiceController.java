package es.ulpgc.tfg.movieshd.controllers;

import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.services.IUserService;
import es.ulpgc.tfg.movieshd.services.InvoiceService;
import es.ulpgc.tfg.movieshd.utils.MathTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Locale;

@Controller
public class InvoiceController {

    private final InvoiceService invoiceService;
    private final IUserService userService;
    private final MessageSource messageSource;

    @Autowired
    public InvoiceController(InvoiceService invoiceService, IUserService userService, MessageSource messageSource) {
        this.invoiceService = invoiceService;
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @GetMapping("/invoice/{id}")
    public String invoiceDetail(@PathVariable(value = "id") String id,
                                Model model, Authentication authentication, RedirectAttributes flash, Locale locale) {

        if (!MathTool.isNumeric(id)) {
            flash.addFlashAttribute("error", messageSource.getMessage("text.invoice.detail.error", null, locale));
            return "redirect:/my-invoices";
        }

        Invoice invoice = invoiceService.getInvoiceByIdAndUser(Long.parseLong(id), userService.findUserByEmail(authentication.getName()));
        if (invoice == null) {
            flash.addFlashAttribute("info", messageSource.getMessage("text.invoice.there.is.no", null, locale));
            return "redirect:/my-invoices";
        }
        model.addAttribute("invoice", invoice);
        return "invoices/invoice_detail";
    }

}
