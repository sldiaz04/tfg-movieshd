package es.ulpgc.tfg.movieshd.controllers;

import es.ulpgc.tfg.movieshd.entities.Favorite;
import es.ulpgc.tfg.movieshd.entities.FavoriteIdentity;
import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.services.IFavoriteService;
import es.ulpgc.tfg.movieshd.services.IMovieService;
import es.ulpgc.tfg.movieshd.utils.SessionUtil;
import es.ulpgc.tfg.movieshd.utils.paginator.PaginationFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/favorite")
public class FavoriteController {

    private final IFavoriteService favoriteService;
    private final IMovieService movieService;
    private final MessageSource messageSource;

    @Autowired
    public FavoriteController(IFavoriteService favoriteService, IMovieService movieService, MessageSource messageSource) {
        this.favoriteService = favoriteService;
        this.movieService = movieService;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/add-movie")
    public @ResponseBody
    Long addFavorite(@RequestParam(name = "id") Long movieId, Authentication authentication, HttpSession session) {

        FavoriteIdentity favoriteIdentity = new FavoriteIdentity(movieId, authentication.getName());
        Favorite favorite = new Favorite(favoriteIdentity);
        favoriteService.save(favorite);

        Movie favoriteMovie = movieService.findById(movieId);
        favoriteMovie.setTotalFavorites(favoriteMovie.getTotalFavorites() + 1);
        movieService.save(favoriteMovie);

        List<Movie> favoriteMovies = SessionUtil.getFavoriteMoviesFromSession(session);
        if (favoriteMovies == null || favoriteMovies.isEmpty()) {// because the list from session is immutable
            favoriteMovies = new ArrayList<>();
        }

        favoriteMovies.add(favoriteMovie);
        session.setAttribute("favoriteMovies", favoriteMovies);

        return favoriteService.totalFavoritesByUserEmail(authentication.getName());
    }

    @GetMapping(value = "/remove-movie")
    public @ResponseBody
    Long removeFavorite(@RequestParam(name = "id") Long movieId, Authentication authentication, HttpSession session) {

        FavoriteIdentity favoriteIdentity = new FavoriteIdentity(movieId, authentication.getName());
        Favorite favorite = new Favorite(favoriteIdentity);
        favoriteService.delete(favorite);

        Movie favoriteMovie = movieService.findById(movieId);
        favoriteMovie.setTotalFavorites(favoriteMovie.getTotalFavorites() - 1);
        movieService.save(favoriteMovie);

        List<Movie> favoriteMovies = SessionUtil.getFavoriteMoviesFromSession(session);
        favoriteMovies.remove(favoriteMovie);
        session.setAttribute("favoriteMovies", favoriteMovies);

        return favoriteService.totalFavoritesByUserEmail(authentication.getName());
    }

    @GetMapping("/all")
    public String allFavorites(@RequestParam(name = "page", defaultValue = "0") int page,
                               Model model, Authentication authentication, Device device, Locale locale) {
        Page<Movie> movies = favoriteService.getAllFavoriteMovies(authentication.getName(), PaginationFactory.createPageRequest(page));
        PaginationFactory.addAttributesToModel(model, messageSource.getMessage("text.user.favorite.movies", null, locale), "/favorite/all", movies, device);
        return "user/favorite_movies";
    }
}
