package es.ulpgc.tfg.movieshd.controllers;

import es.ulpgc.tfg.movieshd.aws.SignedURL;
import es.ulpgc.tfg.movieshd.dto.UserDTO;
import es.ulpgc.tfg.movieshd.entities.ContactInformation;
import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.entities.User;
import es.ulpgc.tfg.movieshd.jwt.JwtFactory;
import es.ulpgc.tfg.movieshd.jwt.JwtSecretType;
import es.ulpgc.tfg.movieshd.services.IMovieService;
import es.ulpgc.tfg.movieshd.services.IUserService;
import es.ulpgc.tfg.movieshd.utils.paginator.PaginationFactory;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jets3t.service.CloudFrontServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.mobile.device.Device;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

@Controller
@SessionAttributes("userDTO")
public class UserController {
    private final IUserService userService;
    private final IMovieService movieService;
    private final BCryptPasswordEncoder passwordEncoder;
    private MessageSource messageSource;

    private final Log logger = LogFactory.getLog(this.getClass());

    private static final String ERROR_FLASH_ATTRIBUTE = "error";
    private static final String SUCCESS_FLASH_ATTRIBUTE = "success";
    private static final String REDIRECT_PROFILE = "redirect:/profile";
    private static final String USER_PROFILE = "user/profile";

    @Autowired
    public UserController(IUserService userService, IMovieService movieService,
                          BCryptPasswordEncoder passwordEncoder, MessageSource messageSource) {
        this.userService = userService;
        this.movieService = movieService;
        this.passwordEncoder = passwordEncoder;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/register")
    public String createUser(Model model) {
        UserDTO dto = new UserDTO();
        model.addAttribute("userDTO", dto);
        return "register";
    }

    @PostMapping("/register")
    public String saveUser(@Valid UserDTO dto, BindingResult result, HttpServletRequest request,
                           RedirectAttributes flash, SessionStatus status, Locale locale) {
        if (result.hasErrors()) {
            return "register";
        }
        User user = userService.findUserByEmail(dto.getEmail());

        if (user != null) {
            String message = String.format(messageSource.getMessage("text.registration.error", null, locale), dto.getEmail());
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, message);
            logger.error("Error in the registry: the user " + dto.getEmail() + " already exist");
            return "redirect:/login";
        }
        user = new User();
        user.setName(dto.getName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());
        userService.registerNewUserAccount(user);

        // Auto login
        try {
            request.login(dto.getEmail(), dto.getPassword());
        } catch (ServletException e) {
            logger.error("Error in autologin: " + e.getMessage());
        }
        status.setComplete();
        flash.addFlashAttribute(SUCCESS_FLASH_ATTRIBUTE, messageSource.getMessage("text.registration.success", null, locale));
        return "redirect:/";
    }

    @Secured("ROLE_USER")
    @GetMapping("my-movies")
    public String myMovies(@RequestParam(name = "page", defaultValue = "0") int page,
                           Model model, Authentication auth, Device device, Locale locale) {
        Page<Movie> movies = userService.getAllMyMovies(auth.getName(), PaginationFactory.createPageRequest(page));
        PaginationFactory.addAttributesToModel(model,
                messageSource.getMessage("text.user.my.movies", null, locale), "/my-movies", movies, device);
        return "user/movies";
    }

    @Secured("ROLE_USER")
    @PostMapping("/watch")
    public String player(@RequestParam(value = "movieId") Long movieId) {
        return "redirect:/watch/" + JwtFactory.generateToken(movieId.toString(), JwtSecretType.SECRET_BASE64, 4);
    }

    @Secured("ROLE_USER")
    @GetMapping("/watch/{token}")
    public String watch(@PathVariable("token") String token, Model model, Device device,
                        Locale locale, RedirectAttributes flash) throws CloudFrontServiceException, IOException {
        Claims claims;
        try {
            claims = JwtFactory.validateToken(token, JwtSecretType.SECRET_BASE64);
        } catch (ExpiredJwtException e) {
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.player.token.expired", null, locale));
            return "redirect:/my-movies";
        } catch (MalformedJwtException | SignatureException e) {
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.player.token.invalid", null, locale));
            return "redirect:/my-movies";
        }

        Movie movie = movieService.findById(Long.parseLong(claims.getSubject()));
        movie.setTotalViews(movie.getTotalViews() + 1);
        movieService.save(movie);

        movie.setVideo720pURL(SignedURL.makeSignedURL(movie.getVideo720pURL()));
        movie.setVideo1080pURL(SignedURL.makeSignedURL(movie.getVideo1080pURL()));

        if (device.isMobile()) {
            model.addAttribute("isMobile", true);
        }

        model.addAttribute("movie", movie);
        model.addAttribute("locale", locale.getLanguage());
        return "user/player";
    }

    @Secured("ROLE_USER")
    @GetMapping("my-invoices")
    public String myInvoices(Model model, Authentication auth) {
        List<Invoice> invoices = userService.getMyInvoices(userService.findUserByEmail(auth.getName()));
        model.addAttribute("invoices", invoices);
        return "invoices/invoice";
    }

    @Secured("ROLE_USER")
    @GetMapping("profile")
    public String myProfile(Model model, Authentication authentication) {
        User user = userService.findUserByEmail(authentication.getName());

        UserDTO dto = new UserDTO();
        dto.setName(user.getName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());

        ContactInformation information = user.getContactInformation();
        if (information != null) {
            dto.setAddress(information.getAddress());
            dto.setCity(information.getCity());
            dto.setCountry(information.getCountry());
            dto.setPostalCode(information.getPostalCode());
            dto.setTelephone(information.getTelephone());
        }

        model.addAttribute("userDTO", dto);
        return USER_PROFILE;
    }

    @Secured("ROLE_USER")
    @PostMapping("/user/profile")
    public String editUserProfile(@Valid UserDTO dto, BindingResult result, Authentication authentication,
                                  RedirectAttributes flash, SessionStatus status, Locale locale) {
        if (result.hasErrors()) {
            return USER_PROFILE;
        }

        User user = userService.findUserByEmail(authentication.getName());
        user.setName(dto.getName());
        user.setLastName(dto.getLastName());
        userService.updateUser(user);

        status.setComplete();
        flash.addFlashAttribute(SUCCESS_FLASH_ATTRIBUTE, messageSource.getMessage("text.user.change.data.success", null, locale));
        return REDIRECT_PROFILE;
    }

    @Secured("ROLE_USER")
    @PostMapping("/user/password")
    public String editUserPassword(@RequestParam(name = "password1") String password1,
                                   @RequestParam(name = "password2") String password2,
                                   RedirectAttributes flash, Authentication authentication,
                                   SessionStatus status, Locale locale) {

        if (!password1.equals(password2)) {
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.user.change.password.error", null, locale));
            return REDIRECT_PROFILE;
        }

        User authenticatedUser = userService.findUserByEmail(authentication.getName());
        authenticatedUser.setPassword(passwordEncoder.encode(password1));
        userService.updateUser(authenticatedUser);

        status.setComplete();
        flash.addFlashAttribute(SUCCESS_FLASH_ATTRIBUTE, messageSource.getMessage("text.user.change.password.success", null, locale));
        return REDIRECT_PROFILE;
    }

    @Secured("ROLE_USER")
    @PostMapping("/user/contact-information")
    public String editUserContactInformation(@Valid UserDTO dto, BindingResult result, Authentication authentication,
                                             RedirectAttributes flash, SessionStatus status, Locale locale) {
        if (result.hasErrors()) {
            return USER_PROFILE;
        }

        User user = userService.findUserByEmail(authentication.getName());
        ContactInformation information = new ContactInformation();
        information.setAddress(dto.getAddress());
        information.setCity(dto.getCity());
        information.setCountry(dto.getCountry());
        information.setPostalCode(dto.getPostalCode());
        information.setTelephone(dto.getTelephone());

        user.setContactInformation(information);
        userService.updateUser(user);

        status.setComplete();
        flash.addFlashAttribute(SUCCESS_FLASH_ATTRIBUTE, messageSource.getMessage("text.user.change.data.success", null, locale));

        return REDIRECT_PROFILE;
    }
}
