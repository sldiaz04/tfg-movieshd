package es.ulpgc.tfg.movieshd.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.Locale;

@Controller
public class LoginController {

    private final Log logger = LogFactory.getLog(this.getClass());
    private MessageSource messageSource;

    @Autowired
    public LoginController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping("/login")
    public String login(@RequestParam(value = "error", required = false) String error,
                        @RequestParam(value = "logout", required = false) String logout,
                        Model model, Principal principal, RedirectAttributes flash, Locale locale) {
        if (principal != null) {
            logger.info("El usuario ya ha iniciado la sesión");
            return "redirect:/";
        }
        if (error != null) {
            logger.info("Error al iniciar sesión");
            flash.addFlashAttribute("error", messageSource.getMessage("text.login.error", null, locale));
            return "redirect:/login";
        }
        if (logout != null) {
            logger.info("El usuario cerró sesión con éxito");
            flash.addFlashAttribute("success", messageSource.getMessage("text.login.logout", null, locale));
            return "redirect:/login";
        }
        return "login";
    }
}
