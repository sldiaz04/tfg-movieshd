package es.ulpgc.tfg.movieshd.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
public class ErrorPageController implements ErrorController {

    private final MessageSource messageSource;

    private static final String ERROR_MESSAGE = "errorMessage";

    @Autowired
    public ErrorPageController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping("/error")
    public String handleError(HttpServletRequest request, Model model, Locale locale) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
            model.addAttribute("statusCode", statusCode);

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                model.addAttribute(ERROR_MESSAGE, messageSource.getMessage("text.page.not.found", null, locale));
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                model.addAttribute(ERROR_MESSAGE, messageSource.getMessage("text.page.internal.server.error", null, locale));
            } else {
                model.addAttribute(ERROR_MESSAGE, messageSource.getMessage("text.page.default.error", null, locale));
            }
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
