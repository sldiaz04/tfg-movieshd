package es.ulpgc.tfg.movieshd.controllers;

import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.services.IMovieService;
import es.ulpgc.tfg.movieshd.utils.Cart;
import es.ulpgc.tfg.movieshd.utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/cart")
public class CartController {

    private final IMovieService movieService;

    private static final String MOVIES_ATTRIBUTE = "movies";

    @Autowired
    public CartController(IMovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/all")
    public String cartDetail(Model model, HttpSession session) {
        List<Movie> movies = SessionUtil.getMoviesCartFromSession(session);
        if (movies != null) {
            BigDecimal totalPrice = new BigDecimal(0);
            for (Movie movie : movies) {
                totalPrice = totalPrice.add(movie.getPrice());
            }
            model.addAttribute("totalPrice", totalPrice);
        }
        return "cart_detail";
    }

    @GetMapping(value = "/add-movie", produces = "application/json")
    public @ResponseBody
    Cart addMovie(@RequestParam(value = "id") Long id, HttpSession session) {
        Movie movie = movieService.findById(id);
        List<Movie> movies = SessionUtil.getMoviesCartFromSession(session);
        if (movies == null) {
            movies = new ArrayList<>();
        }
        if (!movies.contains(movie)) {
            movies.add(movie);
        }
        session.setAttribute(MOVIES_ATTRIBUTE, movies);
        return loadCart(movies.size(), movie);
    }

    @GetMapping(value = "/remove-movie", produces = "application/json")
    public @ResponseBody
    Cart removeMovie(@RequestParam(value = "id") Long id, HttpSession session) {
        Movie movie = movieService.findById(id);
        List<Movie> movies = SessionUtil.getMoviesCartFromSession(session);
        movies.remove(movie);
        session.setAttribute(MOVIES_ATTRIBUTE, movies);
        return loadCart(movies.size(), movie);
    }

    @GetMapping("/empty")
    public @ResponseBody
    Integer emptyCart(HttpSession session) {
        session.removeAttribute(MOVIES_ATTRIBUTE);
        return 0;
    }

    private Cart loadCart(Integer items, Movie movie) {
        Cart cart = new Cart();
        cart.setItems(items);
        cart.setMovie(movie);
        return cart;
    }
}
