package es.ulpgc.tfg.movieshd;

import es.ulpgc.tfg.movieshd.auth.LoginSuccessHandler;
import es.ulpgc.tfg.movieshd.services.JpaUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

@Configuration
@Profile("prod")
@EnableGlobalMethodSecurity(securedEnabled = true)
public class ProductionSpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JpaUserDetailsService userDetailsService;

    private final BCryptPasswordEncoder passwordEncoder;

    private final LoginSuccessHandler successHandler;

    @Autowired
    public ProductionSpringSecurityConfig(JpaUserDetailsService userDetailsService, BCryptPasswordEncoder passwordEncoder, LoginSuccessHandler successHandler) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.successHandler = successHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requiresChannel()
                .anyRequest().requiresSecure()
                .and()
                .authorizeRequests()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                .antMatchers("/", "/error", "/movie/**", "/movies/**", "/register/**", "/cart/**", "/jwt/**", "/firebase/**", "/info/**", "/locale").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().successHandler(successHandler).loginPage("/login").permitAll()
                .and()
                .logout().permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/error_403")
                .and()
                .csrf().csrfTokenRepository(new HttpSessionCsrfTokenRepository());
    }

    @Autowired
    public void configurerGlobal(AuthenticationManagerBuilder builder) throws Exception {
        // Configuracion del acceso con roles en JPA
        builder.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }
}
