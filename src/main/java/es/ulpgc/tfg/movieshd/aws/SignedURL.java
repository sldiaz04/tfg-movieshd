package es.ulpgc.tfg.movieshd.aws;

import org.jets3t.service.CloudFrontService;
import org.jets3t.service.CloudFrontServiceException;
import org.jets3t.service.utils.ServiceUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Date;

public class SignedURL {

    private static final String HTTPS = "https://";
    private static final String DISTRIBUTION_DOMAIN = "dsui1eartjtrz.cloudfront.net/";
    private static final Long EXPIRATION_TIME_HOUR = 3600000L;

    private SignedURL() {
    }

    public static String makeSignedURL(String s3ObjectKey) throws IOException, CloudFrontServiceException {

        if (s3ObjectKey == null) return s3ObjectKey;

        String policyResourcePath = HTTPS + DISTRIBUTION_DOMAIN + s3ObjectKey;

        // Convert your DER file into a byte array.
        byte[] derPrivateKey = ServiceUtils.readInputStreamToBytes(getPk().getInputStream());

        String policy = CloudFrontService.buildPolicyForSignedUrl(
                // Resource path (optional, can include '*' and '?' wildcards)
                policyResourcePath,
                // DateLessThan
                new Date(System.currentTimeMillis() + EXPIRATION_TIME_HOUR * 4),
                // CIDR IP address restriction (optional, 0.0.0.0/0 means everyone)
                "0.0.0.0/0",
                // DateGreaterThan (optional)
                null//ServiceUtils.parseIso8601Date("2011-10-16T06:31:56.000Z")
        );

        // Generate a signed URL using a custom policy document.
        return CloudFrontService.signUrl(
                // Resource URL or Path
                policyResourcePath,
                // Certificate identifier, an active trusted signer for the distribution
                "APKAJZWCQHG5TBGX2HLQ",
                // DER Private key data
                derPrivateKey,
                // Access control policy
                policy
        );
    }

    private static Resource getPk() {
        return new ClassPathResource("config/pk-APKAJZWCQHG5TBGX2HLQ.der");
    }

}
