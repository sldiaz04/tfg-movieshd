package es.ulpgc.tfg.movieshd;

import com.braintreegateway.BraintreeGateway;
import es.ulpgc.tfg.movieshd.braintreegateway.BraintreeGatewayFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class MovieshdApplication {

    private static final Log logger = LogFactory.getLog(MovieshdApplication.class);
    private static BraintreeGateway gateway;

    public static void main(String[] args) {
        loadBraintreeConfiguration();
        SpringApplication.run(MovieshdApplication.class, args);
    }

    public static BraintreeGateway getGateway() {
        return gateway;
    }

    private static void loadBraintreeConfiguration() {
        Resource configFile = new ClassPathResource("config/config.properties");
        try {
            if (configFile.exists() && configFile.isFile()) {
                gateway = BraintreeGatewayFactory.fromConfigFile(configFile);
            } else {
                gateway = BraintreeGatewayFactory.fromConfigMapping(System.getenv());
            }
        } catch (NullPointerException e) {
            logger.error("Could not load Braintree configuration from config file or system environment", e);
            System.exit(1);
        }
    }

}
