package es.ulpgc.tfg.movieshd.view.pdf;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import es.ulpgc.tfg.movieshd.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component("invoices/invoice_detail")
public class InvoicePdfView extends AbstractPdfView {
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    @Autowired
    public InvoicePdfView(MessageSource messageSource, LocaleResolver localeResolver) {
        this.messageSource = messageSource;
        this.localeResolver = localeResolver;
    }

    @Override
    protected void buildPdfDocument(Map<String, Object> map, Document document, PdfWriter pdfWriter,
                                    HttpServletRequest request, HttpServletResponse response) throws Exception {
        Invoice invoice = (Invoice) map.get("invoice");
        Locale locale = localeResolver.resolveLocale(request);

        String message = messageSource.getMessage("text.pdf.name", null, locale);
        String invoiceName = message.concat("-").concat(invoice.getId().toString()).concat(".pdf");

        response.setHeader("Content-Disposition", "attachment; filename=".concat(invoiceName));// File name

        // Styles
        Color blueColor = new Color(49, 97, 163);
        Font informationFont = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, blueColor);
        Font headerFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, blueColor);
        Font bodyFont = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0));
        Font footerFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, blueColor);

        LineSeparator separator = new LineSeparator();
        separator.setLineColor(blueColor);

        // Company Information
        PdfPTable companyInformationTable = makeTableOfInformation();
        setTableProperties(companyInformationTable);

        PdfPCell cell = new PdfPCell();
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.addElement(addCompanyInformation());
        companyInformationTable.addCell(cell);

        // Company Logo
        cell = new PdfPCell();
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell.addElement(addCompanyLogo());
        companyInformationTable.addCell(cell);

        document.add(companyInformationTable);

        // Invoice Information Table
        document.add(makeParagraph(messageSource.getMessage("text.pdf.invoice.information", null, locale), informationFont));
        document.add(separator);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
                .withLocale(locale);

        PdfPTable transactionInformationTable = makeTableOfInformation();
        transactionInformationTable.setWidths(new float[]{1, 3f});
        transactionInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.invoice.id", null, locale)));
        transactionInformationTable.addCell(makeCell(invoice.getId().toString()));
        transactionInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.invoice.date", null, locale)));
        transactionInformationTable.addCell(makeCell(invoice.getCreateAt().format(dateTimeFormatter)));
        transactionInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.invoice.amount", null, locale)));
        transactionInformationTable.addCell(makeCell(invoice.getPriceWithTaxes().toString() + " €"));
        document.add(transactionInformationTable);

        // Customer Information Table
        document.add(makeParagraph(messageSource.getMessage("text.pdf.customer.information", null, locale), informationFont));
        document.add(separator);
        User user = invoice.getUser();
        ContactInformation contactInformation = user.getContactInformation();

        PdfPTable customerInformationTable = makeTableOfInformation();
        customerInformationTable.setWidths(new float[]{1, 3f});
        customerInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.customer.name", null, locale)));
        customerInformationTable.addCell(makeCell(user.getFullName()));
        customerInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.customer.email", null, locale)));
        customerInformationTable.addCell(makeCell(user.getEmail()));
        if (contactInformation != null) {
            customerInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.customer.phone", null, locale)));
            customerInformationTable.addCell(makeCell(contactInformation.getTelephone()));
            customerInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.customer.address", null, locale)));
            customerInformationTable.addCell(makeCell(contactInformation.getAddress()));
            customerInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.customer.postalcode", null, locale)));
            customerInformationTable.addCell(makeCell(contactInformation.getPostalCode().toString()));
            customerInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.customer.city", null, locale)));
            customerInformationTable.addCell(makeCell(contactInformation.getCity()));
            customerInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.customer.country", null, locale)));
            customerInformationTable.addCell(makeCell(contactInformation.getCountry()));
        }
        document.add(customerInformationTable);

        // Payment Information Table
        document.add(makeParagraph(messageSource.getMessage("text.pdf.payment.information", null, locale), informationFont));
        document.add(separator);

        PaymentInformation paymentInformation = invoice.getPaymentInformation();
        PaymentType paymentType = paymentInformation.getPaymentType();

        PdfPTable paymentInformationTable = makeTableOfInformation();
        paymentInformationTable.setWidths(new float[]{1, 2f});
        paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.type", null, locale)));
        paymentInformationTable.addCell(makeCell(paymentType.toString()));

        if (paymentType.equals(PaymentType.PAYPAL)) {
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.payer.email", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getPayerEmail()));
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.payer.id", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getPayerId()));
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.payer.first.name", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getPayerFirstName()));
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.payer.last.name", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getPayerLastName()));
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.id", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getPaymentId()));
        } else if (paymentType.equals(PaymentType.CREDIT_CARD)) {
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.card.holder", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getCardHolderName()));
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.card.type", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getCardType()));
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.card.number", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getCardNumber()));
            paymentInformationTable.addCell(makeCell(messageSource.getMessage("text.pdf.payment.card.expiration", null, locale)));
            paymentInformationTable.addCell(makeCell(paymentInformation.getCardExpiration()));
        }
        document.add(paymentInformationTable);

        // Line Items Table
        document.add(makeParagraph(messageSource.getMessage("text.pdf.items", null, locale), informationFont));
        document.add(separator);

        PdfPTable lineItemsTable = makeTableOfItems();
        lineItemsTable.addCell(makeCellToLineItems(messageSource.getMessage("text.pdf.items.title", null, locale), PdfPCell.ALIGN_LEFT, headerFont));
        lineItemsTable.addCell(makeCellToLineItems(messageSource.getMessage("text.pdf.items.quantity", null, locale), PdfPCell.ALIGN_CENTER, headerFont));
        lineItemsTable.addCell(makeCellToLineItems(messageSource.getMessage("text.pdf.items.unit.amount", null, locale), PdfPCell.ALIGN_RIGHT, headerFont));
        lineItemsTable.addCell(makeCellToLineItems(messageSource.getMessage("text.pdf.items.total.amount", null, locale), PdfPCell.ALIGN_RIGHT, headerFont));

        for (InvoiceItem item : invoice.getItems()) {
            lineItemsTable.addCell(makeCellToLineItems(item.getMovie().getTitle(), PdfPCell.ALIGN_LEFT, bodyFont));
            lineItemsTable.addCell(makeCellToLineItems("1", PdfPCell.ALIGN_CENTER, bodyFont));
            lineItemsTable.addCell(makeCellToLineItems(item.getPriceWithoutTaxes().toString(), PdfPCell.ALIGN_RIGHT, bodyFont));
            lineItemsTable.addCell(makeCellToLineItems(item.getPriceWithoutTaxes().toString() + " €", PdfPCell.ALIGN_RIGHT, bodyFont));
        }

        // Subtotal, Taxes and Total
        Map<String, String> prices = new HashMap<>();
        prices.put("text.pdf.invoice.subtotal", invoice.getPriceWithoutTaxes() + " €");
        prices.put("text.pdf.invoice.taxes", invoice.getPriceWithTaxes().subtract(invoice.getPriceWithoutTaxes()) + " €");
        prices.put("text.pdf.invoice.total", invoice.getPriceWithTaxes() + " €");

        for (Map.Entry<String, String> sets : prices.entrySet()) {
            cell = makeCellToLineItems(messageSource.getMessage(sets.getKey(), null, locale), PdfPCell.ALIGN_RIGHT, footerFont);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setColspan(3);
            cell.setPaddingBottom(3);
            cell.setPaddingTop(3);
            lineItemsTable.addCell(cell);

            cell = makeCellToLineItems(sets.getValue(), PdfPCell.ALIGN_RIGHT, footerFont);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setPaddingBottom(3);
            cell.setPaddingTop(3);
            lineItemsTable.addCell(cell);
        }

        document.add(lineItemsTable);

    }

    private Element addCompanyInformation() {
        Paragraph paragraph = new Paragraph();
        paragraph.setFont(FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(49, 97, 163)));
        paragraph.add(new Phrase("TFG MOVIES-HD\n"));
        paragraph.setFont(FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0)));
        paragraph.add(new Phrase("Ctra. Tafira Baja A\n"));
        paragraph.add(new Phrase("35017\n"));
        paragraph.add(new Phrase("Las Palmas de Gran Canaria\n"));
        paragraph.add(new Phrase("España"));
        return paragraph;
    }

    private Element addCompanyLogo() throws IOException {
        Image image = Image.getInstance("https://d1sjecyzvhtrj9.cloudfront.net/images/tfg-movieshd-logo.png");
        image.setAlt("TFG MOVIES-HD LOGO");
        image.scaleAbsoluteWidth(60);
        image.setAlignment(Image.ALIGN_RIGHT);
        return image;
    }

    private PdfPTable makeTableOfItems() {
        PdfPTable table = new PdfPTable(4);
        table.setWidths(new float[]{3f, 1, 1, 1});
        setTableProperties(table);
        return table;
    }

    private PdfPTable makeTableOfInformation() {
        PdfPTable table = new PdfPTable(2);
        setTableProperties(table);
        return table;
    }

    private void setTableProperties(PdfPTable table) {
        table.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
        table.setWidthPercentage(100);
        table.setSpacingBefore(10);
        table.setSpacingAfter(20);
    }

    private Paragraph makeParagraph(String title, Font font) {
        Paragraph paragraph = new Paragraph(title, font);
        paragraph.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph.setSpacingAfter(10);
        return paragraph;
    }

    private PdfPCell makeCell(String title) {
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase(title, FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0))));
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private PdfPCell makeCellToLineItems(String title, Integer alignItem, Font font) {
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase(title, font));
        cell.setBorder(Rectangle.BOTTOM);
        cell.setHorizontalAlignment(alignItem);
        cell.setPaddingBottom(10);
        cell.setPaddingTop(10);
        return cell;
    }
}
