package es.ulpgc.tfg.movieshd.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class FirebaseConfig implements CommandLineRunner {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public void run(String... args) {
        InputStream serviceAccount;
        FirebaseOptions options = null;
        try {
            serviceAccount = new ClassPathResource("config/movieshd-217923-firebase-adminsdk-wcedd-a098379029.json").getInputStream();
            options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://movieshd-217923.firebaseio.com/")
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            logger.error("Could not configurate the Firebase credentials", e);
            System.exit(1);
        }
    }
}
