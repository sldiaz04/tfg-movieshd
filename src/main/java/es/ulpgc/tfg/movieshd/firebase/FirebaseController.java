package es.ulpgc.tfg.movieshd.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import es.ulpgc.tfg.movieshd.entities.User;
import es.ulpgc.tfg.movieshd.services.IUserService;
import es.ulpgc.tfg.movieshd.utils.SessionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.UUID;

@Controller
@RequestMapping("firebase")
public class FirebaseController {
    private final IUserService userService;
    private final SessionUtil sessionUtil;

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    public FirebaseController(IUserService userService, SessionUtil sessionUtil) {
        this.userService = userService;
        this.sessionUtil = sessionUtil;
    }

    @GetMapping(value = "/idToken")
    public @ResponseBody
    String firebase(@RequestParam(name = "idToken") String idToken, HttpSession session) {
        FirebaseToken decodedToken = null;
        try {
            decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
        } catch (FirebaseAuthException e) {
            logger.error("Error verifying the Firebase idToken", e);
            return "ERROR";
        }

        String userEmail = decodedToken.getEmail();
        String userName = decodedToken.getName();
        String userUid = decodedToken.getUid();
        String userImageProfile = decodedToken.getPicture();

        User user = userService.findUserByEmail(userEmail);
        if (user == null) {
            // The user does not exist
            user = new User();
            user.setName(userName);
            user.setEmail(userEmail);
            user.setAuthClientID(userUid);
            user.setPassword(userUid + UUID.randomUUID().toString());
            user.setImageProfile(userImageProfile);
            userService.registerNewUserAccount(user);
        } else if (user.getAuthClientID() == null) {
            // Normal user
            user.setName(userName);
            user.setLastName("");
            user.setAuthClientID(userUid);
            user.setImageProfile(userImageProfile);
            userService.updateUser(user);
        }

        sessionUtil.autoLogin(user);
        sessionUtil.loadPurchasedMoviesInSession(session, userEmail);
        sessionUtil.loadFavoriteMoviesInSession(session, userEmail);
        sessionUtil.loadUserImageProfileInSession(session, userImageProfile);

        return "OK";
    }


}
