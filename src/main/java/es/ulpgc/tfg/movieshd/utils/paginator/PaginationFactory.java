package es.ulpgc.tfg.movieshd.utils.paginator;

import es.ulpgc.tfg.movieshd.entities.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mobile.device.Device;
import org.springframework.ui.Model;

public class PaginationFactory {

    private PaginationFactory() {
    }

    public static Pageable createPageRequest(int page) {
        return PageRequest.of(page, 12);
    }

    public static void addAttributesToModel(Model model, String title, String url, Page<Movie> movies, Device device) {
        model.addAttribute("title", title);
        model.addAttribute("page", createPageRender(url, movies, device.isMobile() ? 3 : 5));
        model.addAttribute("movies", movies);
    }

    private static PageRender<Movie> createPageRender(String url, Page<Movie> movies, int numOfItemsInPagination) {
        return new PageRender<>(url, movies, numOfItemsInPagination);
    }
}
