package es.ulpgc.tfg.movieshd.utils;

import es.ulpgc.tfg.movieshd.entities.Movie;

public class Cart {
    private Integer items;
    private Movie movie;

    public Integer getItems() {
        return items;
    }

    public void setItems(Integer items) {
        this.items = items;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
