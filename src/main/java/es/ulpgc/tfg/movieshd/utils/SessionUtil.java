package es.ulpgc.tfg.movieshd.utils;

import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.entities.User;
import es.ulpgc.tfg.movieshd.services.IFavoriteService;
import es.ulpgc.tfg.movieshd.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class SessionUtil {

    private final IUserService userService;
    private final IFavoriteService favoriteService;

    @Autowired
    public SessionUtil(IUserService userService, IFavoriteService favoriteService) {
        this.userService = userService;
        this.favoriteService = favoriteService;
    }

    public void autoLogin(User user) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        user.getRoles().forEach(role -> grantedAuthorities.add(new SimpleGrantedAuthority(role.getAuthority())));
        Authentication authentication = new UsernamePasswordAuthenticationToken(user.getEmail(), null, grantedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public void loadPurchasedMoviesInSession(HttpSession session, String authenticatedUser) {
        List<Movie> moviesPurchases = userService.getAllMyMovies(authenticatedUser);
        List<Movie> moviesInCartSession = getMoviesCartFromSession(session);

        // To avoid NullPointerException
        if (moviesInCartSession == null) {
            moviesInCartSession = new ArrayList<>();
        }

        moviesInCartSession.removeAll(moviesPurchases);
        session.setAttribute("moviesPurchases", moviesPurchases);
        session.setAttribute("movies", moviesInCartSession);
    }

    public void loadFavoriteMoviesInSession(HttpSession session, String authenticatedUser) {
        List<Movie> movies = favoriteService.getAllFavoriteMovies(authenticatedUser);
        session.setAttribute("favoriteMovies", movies);
    }

    public void loadUserImageProfileInSession(HttpSession session, String userImage) {
        session.setAttribute("userImage", userImage);
    }

    @SuppressWarnings("unchecked")
    public static List<Movie> getMoviesCartFromSession(HttpSession session) {
        return (List<Movie>) session.getAttribute("movies"); //Type safety: Unchecked cast from Object to Set<Movie>
    }

    @SuppressWarnings("unchecked")
    public static List<Movie> getMoviesPurchasedFromSession(HttpSession session) {
        return (List<Movie>) session.getAttribute("moviesPurchases"); //Type safety: Unchecked cast from Object to Set<Movie>
    }

    @SuppressWarnings("unchecked")
    public static List<Movie> getFavoriteMoviesFromSession(HttpSession session) {
        return (List<Movie>) session.getAttribute("favoriteMovies"); //Type safety: Unchecked cast from Object to Set<Movie>
    }
}
