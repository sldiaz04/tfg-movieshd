package es.ulpgc.tfg.movieshd.utils.paginator;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public class PageRender<T> {
    private String url;
    private int totalPages;
    private int currentPage;
    private Page<T> page;
    private List<PageItem> pages;

    PageRender(String url, Page<T> page, int numOfItemsInPagination) {
        this.url = url;
        this.page = page;
        this.pages = new ArrayList<>();
        this.totalPages = page.getTotalPages();
        this.currentPage = page.getNumber() + 1;

        int fromPage;
        int toPage;

        if (totalPages <= numOfItemsInPagination) {
            fromPage = 1;
            toPage = totalPages;
        } else {
            if (currentPage <= numOfItemsInPagination / 2) {
                fromPage = 1;
                toPage = numOfItemsInPagination;
            } else if (currentPage >= totalPages - numOfItemsInPagination / 2) {
                fromPage = totalPages - numOfItemsInPagination + 1;
                toPage = numOfItemsInPagination;
            } else {
                fromPage = currentPage - numOfItemsInPagination / 2;
                toPage = numOfItemsInPagination;
            }
        }

        for (int i = 0; i < toPage; i++) {
            pages.add(new PageItem(fromPage + i, currentPage == fromPage + i));
        }
    }

    public String getUrl() {
        return url;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public List<PageItem> getPages() {
        return pages;
    }

    public boolean isFirst() {
        return page.isFirst();
    }

    public boolean isLast() {
        return page.isLast();
    }

    public boolean isHasNext() {
        return page.hasNext();
    }

    public boolean isHasPrevious() {
        return page.hasPrevious();
    }
}
