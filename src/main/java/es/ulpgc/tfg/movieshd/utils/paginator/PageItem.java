package es.ulpgc.tfg.movieshd.utils.paginator;

public class PageItem {
    private int num;
    private boolean currentPage;

    PageItem(int num, boolean isCurrentPage) {
        this.num = num;
        this.currentPage = isCurrentPage;
    }

    public int getNum() {
        return num;
    }

    public boolean isCurrentPage() {
        return currentPage;
    }
}
