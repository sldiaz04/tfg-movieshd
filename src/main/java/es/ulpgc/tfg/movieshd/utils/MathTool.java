package es.ulpgc.tfg.movieshd.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathTool {

    private MathTool() {
    }

    public static boolean isNumeric(String id) {
        boolean isNumeric = true;
        try {
            Long.parseLong(id);
        } catch (NumberFormatException e) {
            isNumeric = false;
        }
        return isNumeric;
    }

    public static BigDecimal getBigDecimalWithScale2(BigDecimal price) {
        return price.setScale(2, RoundingMode.HALF_UP);
    }
}
