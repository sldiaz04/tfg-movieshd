package es.ulpgc.tfg.movieshd.jwt;

import com.amazonaws.util.Base32;
import org.springframework.util.Base64Utils;

public enum JwtSecretType {
    SECRET_BASE32(Base32.encodeAsString("SecretKey_258147369".getBytes())),
    SECRET_BASE64(Base64Utils.encodeToString("SecretKey_258147369".getBytes()));

    private String secretValue;

    JwtSecretType(String secretValue) {
        this.secretValue = secretValue;
    }

    public String getSecretValue() {
        return secretValue;
    }
}
