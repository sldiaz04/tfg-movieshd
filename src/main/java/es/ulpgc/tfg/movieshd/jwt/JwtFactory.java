package es.ulpgc.tfg.movieshd.jwt;

import io.jsonwebtoken.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;

public class JwtFactory {
    private static final Long EXPIRATION_TIME_HOUR = 3600000L;

    private static final Log logger = LogFactory.getLog(JwtFactory.class);

    private JwtFactory() {
    }

    public static String generateToken(String subject, JwtSecretType jwtSecretType, int expirationTimeInHour) {
        return Jwts.builder()
                .setSubject(subject)
                .signWith(SignatureAlgorithm.HS512, jwtSecretType.getSecretValue())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME_HOUR * expirationTimeInHour))
                .compact();
    }

    public static Claims validateToken(String token, JwtSecretType jwtSecretType) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(jwtSecretType.getSecretValue())
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException e) {
            logger.error("JwtExceptions: " + e.getMessage());
            throw e;
        }
        return claims;
    }
}
