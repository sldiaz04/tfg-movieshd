package es.ulpgc.tfg.movieshd.jwt;

import es.ulpgc.tfg.movieshd.email.MailClientBuilder;
import es.ulpgc.tfg.movieshd.email.model.Mail;
import es.ulpgc.tfg.movieshd.entities.User;
import es.ulpgc.tfg.movieshd.services.IUserService;
import es.ulpgc.tfg.movieshd.utils.SessionUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("jwt")
public class JwtController {
    private final IUserService userService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final MailClientBuilder mailClientBuilder;
    private final SessionUtil sessionUtil;
    private final Environment environment;
    private MessageSource messageSource;

    private final Log logger = LogFactory.getLog(this.getClass());

    private static final String ERROR_FLASH_ATTRIBUTE = "error";
    private static final String REDIRECT_JWT_SEND_EMAIL = "redirect:/jwt/send-email";

    @Autowired
    public JwtController(IUserService userService, BCryptPasswordEncoder passwordEncoder, MailClientBuilder mailClientBuilder,
                         SessionUtil sessionUtil, Environment environment, MessageSource messageSource) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.sessionUtil = sessionUtil;
        this.mailClientBuilder = mailClientBuilder;
        this.environment = environment;
        this.messageSource = messageSource;
    }

    @GetMapping("send-email")
    public String sendEmail() {
        return "password/email_form";
    }

    @PostMapping("recipient-email")
    public String recipientEmail(@RequestParam(value = "email") String email, RedirectAttributes flash, Locale locale) {
        User user = userService.findUserByEmail(email);
        if (user == null) {
            String message = String.format(messageSource.getMessage("text.user.not.exist", null, locale), email);
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, message);
            return REDIRECT_JWT_SEND_EMAIL;
        }

        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setSubject(messageSource.getMessage("text.email.retrieve.password.subject", null, locale));
        mail.setEmailTemplate("emails/retrieve_password");

        Map<String, Object> params = new HashMap<>();
        String url = "https://www.movies-hd.cf/jwt/";

        if (environment.getActiveProfiles()[0].equals("dev")) {
            url = "http://localhost:8080/jwt/";
        }

        String token = JwtFactory.generateToken(email, JwtSecretType.SECRET_BASE32, 1);

        params.put("user", user.getFullName());
        params.put("url", url + token);
        mailClientBuilder.prepareAndSend(mail, params, locale);

        flash.addFlashAttribute("info", messageSource.getMessage("text.user.email.sent", null, locale));
        return "redirect:/";
    }

    @GetMapping("/{token}")
    public String token(@PathVariable(value = "token") String token, Model model, RedirectAttributes flash, Locale locale) {

        Claims claims;
        try {
            claims = JwtFactory.validateToken(token, JwtSecretType.SECRET_BASE32);
        } catch (ExpiredJwtException e) {
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.expired.token", null, locale));
            return REDIRECT_JWT_SEND_EMAIL;
        } catch (MalformedJwtException | SignatureException e) {
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.invalid.token", null, locale));
            return REDIRECT_JWT_SEND_EMAIL;
        }

        model.addAttribute("email", claims.getSubject());
        model.addAttribute("token", token);
        return "password/retrieve_form";
    }

    @PostMapping("change-password")
    public String changePassword(@RequestParam(value = "password1") String password1,
                                 @RequestParam(value = "password2") String password2,
                                 @RequestParam(value = "token") String token,
                                 @RequestParam(value = "email") String email,
                                 RedirectAttributes flash, HttpServletRequest request, Locale locale) {

        if (!password1.equals(password2)) {
            flash.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.user.change.password.error", null, locale));
            return "redirect:/jwt/" + token;
        }

        User user = userService.findUserByEmail(email);
        user.setPassword(passwordEncoder.encode(password1));
        userService.updateUser(user);

        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setSubject(messageSource.getMessage("text.email.change.password.subject", null, locale));
        mail.setEmailTemplate("emails/password_changed");

        Map<String, Object> params = new HashMap<>();

        params.put("user", user.getFullName());
        mailClientBuilder.prepareAndSend(mail, params, locale);

        // Auto login
        try {
            request.login(user.getEmail(), password1);
            sessionUtil.loadPurchasedMoviesInSession(request.getSession(), user.getEmail());
            sessionUtil.loadFavoriteMoviesInSession(request.getSession(), user.getEmail());
        } catch (ServletException e) {
            logger.error("Error of autologin: " + e.getMessage());
        }

        flash.addFlashAttribute("success", messageSource.getMessage("text.user.change.password.success", null, locale));
        return "redirect:/";
    }
}
