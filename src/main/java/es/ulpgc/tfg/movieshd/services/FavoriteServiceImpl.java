package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.dao.IFavoriteDao;
import es.ulpgc.tfg.movieshd.entities.Favorite;
import es.ulpgc.tfg.movieshd.entities.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteServiceImpl implements IFavoriteService {
    private IFavoriteDao favoriteDao;

    @Autowired
    public FavoriteServiceImpl(IFavoriteDao favoriteDao) {
        this.favoriteDao = favoriteDao;
    }

    @Override
    public void save(Favorite favorite) {
        favoriteDao.save(favorite);
    }

    @Override
    public void delete(Favorite favorite) {
        favoriteDao.delete(favorite);
    }

    @Override
    public Long totalFavoritesByUserEmail(String userEmail) {
        return favoriteDao.countFavoriteByFavoriteIdentity_UserEmail(userEmail);
    }

    @Override
    public List<Movie> getAllFavoriteMovies(String userEmail) {
        return favoriteDao.getAllFavoriteMovies(userEmail);
    }

    @Override
    public Page<Movie> getAllFavoriteMovies(String userEmail, Pageable pageable) {
        return favoriteDao.getAllFavoriteMovies(userEmail, pageable);
    }
}
