package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.entities.Favorite;
import es.ulpgc.tfg.movieshd.entities.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IFavoriteService {
    void save(Favorite favorite);

    void delete(Favorite favorite);

    Long totalFavoritesByUserEmail(String userEmail);

    List<Movie> getAllFavoriteMovies(String userEmail);

    Page<Movie> getAllFavoriteMovies(String userEmail, Pageable pageable);

}
