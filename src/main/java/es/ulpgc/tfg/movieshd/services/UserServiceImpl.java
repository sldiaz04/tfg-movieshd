package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.dao.IUserDao;
import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.entities.Role;
import es.ulpgc.tfg.movieshd.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    private final IUserDao userDao;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(IUserDao userDao, BCryptPasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void registerNewUserAccount(User user) {
        Role role = new Role();
        role.setAuthority("ROLE_USER");
        user.setRoles(Collections.singletonList(role));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    @Override
    public User findUserByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public List<Movie> getAllMyMovies(String userEmail) {
        return userDao.fetchMyMovies(userEmail);
    }

    @Override
    public Page<Movie> getAllMyMovies(String userEmail, Pageable pageable) {
        return userDao.fetchMyMovies(userEmail, pageable);
    }

    @Override
    public List<Invoice> getMyInvoices(User user) {
        return userDao.fetchMyInvoices(user);
    }

    @Override
    public void updateUser(User user) {
        userDao.save(user);
    }
}
