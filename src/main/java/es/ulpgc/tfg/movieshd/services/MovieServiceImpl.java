package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.dao.IMovieDao;
import es.ulpgc.tfg.movieshd.entities.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MovieServiceImpl implements IMovieService {

    private final IMovieDao movieDao;

    @Autowired
    public MovieServiceImpl(IMovieDao movieDao) {
        this.movieDao = movieDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Movie> findByName(String term) {
        return movieDao.findByTitleLikeIgnoreCase("%" + term + "%");
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Movie> findByName(String term, Pageable pageable) {
        return movieDao.findByTitleLikeIgnoreCase("%" + term + "%", pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Movie> findFirst10ByOrderByTotalPurchasesDesc() {
        return movieDao.findFirst10ByOrderByTotalPurchasesDesc();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Movie> findFirst10ByOrderByTotalFavoritesDesc() {
        return movieDao.findFirst10ByOrderByTotalFavoritesDesc();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Movie> findFirst10ByOrderByTotalViewsDesc() {
        return movieDao.findFirst10ByOrderByTotalViewsDesc();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Movie> findFirst4ByOrderByTotalFavoritesDesc() {
        return movieDao.findFirst4ByOrderByTotalFavoritesDesc();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Movie> findByLanguage(String language, Pageable pageable) {
        return movieDao.findByLanguageIgnoreCase(language, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Movie> findMoviesByGenre(String genre, Pageable pageable) {
        return movieDao.findMoviesByGenre(genre, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Movie findById(Long id) {
        return movieDao.findById(id).orElse(null);
    }

    @Override
    public void save(Movie movie) {
        movieDao.save(movie);
    }

    @Override
    public void saveAll(List<Movie> movies) {
        movieDao.saveAll(movies);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Movie> findByOrderByTotalFavoritesDesc(Pageable pageable) {
        return movieDao.findByOrderByTotalFavoritesDesc(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Movie> findByOrderByTotalPurchasesDesc(Pageable pageable) {
        return movieDao.findByOrderByTotalPurchasesDesc(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Movie> findByOrderByTotalViewsDesc(Pageable pageable) {
        return movieDao.findByOrderByTotalViewsDesc(pageable);
    }
}
