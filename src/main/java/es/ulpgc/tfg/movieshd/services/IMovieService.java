package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.entities.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IMovieService {
    Movie findById(Long id);

    void save(Movie movie);

    void saveAll(List<Movie> movies);

    List<Movie> findByName(String term);

    Page<Movie> findByName(String term, Pageable pageable);

    Page<Movie> findByLanguage(String language, Pageable pageable);

    Page<Movie> findMoviesByGenre(String genre, Pageable pageable);

    List<Movie> findFirst4ByOrderByTotalFavoritesDesc();

    List<Movie> findFirst10ByOrderByTotalFavoritesDesc();

    Page<Movie> findByOrderByTotalFavoritesDesc(Pageable pageable);

    List<Movie> findFirst10ByOrderByTotalPurchasesDesc();

    Page<Movie> findByOrderByTotalPurchasesDesc(Pageable pageable);

    List<Movie> findFirst10ByOrderByTotalViewsDesc();

    Page<Movie> findByOrderByTotalViewsDesc(Pageable pageable);

}
