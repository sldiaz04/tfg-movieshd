package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IUserService {
    void registerNewUserAccount(User user);

    User findUserByEmail(String email);

    List<Movie> getAllMyMovies(String userEmail);

    Page<Movie> getAllMyMovies(String userEmail, Pageable pageable);

    List<Invoice> getMyInvoices(User user);

    void updateUser(User user);
}
