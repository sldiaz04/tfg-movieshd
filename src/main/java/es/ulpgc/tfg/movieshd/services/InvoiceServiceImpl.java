package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.dao.InvoiceDao;
import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    private final InvoiceDao invoiceDao;

    @Autowired
    public InvoiceServiceImpl(InvoiceDao invoiceDao) {
        this.invoiceDao = invoiceDao;
    }

    @Override
    public void saveInvoice(Invoice invoice) {
        invoiceDao.save(invoice);
    }

    @Override
    public Invoice getInvoiceByIdAndUser(Long id, User user) {
        return invoiceDao.fetchInvoiceWithItems(id, user);
    }
}
