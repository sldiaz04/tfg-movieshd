package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.entities.User;

public interface InvoiceService {
    void saveInvoice(Invoice invoice);

    Invoice getInvoiceByIdAndUser(Long id, User user);
}
