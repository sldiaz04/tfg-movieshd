package es.ulpgc.tfg.movieshd.services;

import es.ulpgc.tfg.movieshd.entities.Role;
import es.ulpgc.tfg.movieshd.entities.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    private final IUserService userService;

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    public JpaUserDetailsService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) {
        User user = userService.findUserByEmail(email);

        if (user == null) {
            logger.error("Error login: no existe el usuario " + email);
            throw new UsernameNotFoundException("Usuario " + email + " no existe");
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Role role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
            logger.info("Role: ".concat(role.getAuthority()));
        }
        if (authorities.isEmpty()) {
            logger.info("Error login: usuario" + email + " no tiene roles asignados");
            throw new UsernameNotFoundException("Usuario " + email + " no tiene roles");
        }

        return new org.springframework.security.core.userdetails.User(email, user.getPassword(), authorities);
    }
}
