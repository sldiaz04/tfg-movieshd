package es.ulpgc.tfg.movieshd.dao;

import es.ulpgc.tfg.movieshd.entities.Favorite;
import es.ulpgc.tfg.movieshd.entities.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IFavoriteDao extends CrudRepository<Favorite, Long> {
    Long countFavoriteByFavoriteIdentity_UserEmail(String userEmail);

    @Query("select m from Movie m join Favorite f on m.id = f.favoriteIdentity.movieId and f.favoriteIdentity.userEmail = ?1")
    List<Movie> getAllFavoriteMovies(String userEmail);

    @Query("select m from Movie m, Favorite f where m.id = f.favoriteIdentity.movieId and f.favoriteIdentity.userEmail = :userEmail")
    Page<Movie> getAllFavoriteMovies(@Param("userEmail") String userEmail, Pageable pageable);
}
