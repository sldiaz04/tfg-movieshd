package es.ulpgc.tfg.movieshd.dao;

import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.entities.Movie;
import es.ulpgc.tfg.movieshd.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IUserDao extends CrudRepository<User, Long> {

    User findByEmail(String email);

    @Query("select i from Invoice i where i.user = ?1")
    List<Invoice> fetchMyInvoices(User user);

    @Query("select distinct m from Movie m, InvoiceItem it, Invoice i where m.id = it.movie.id and i.user.email = ?1 and i.id = it.invoice.id")
    List<Movie> fetchMyMovies(String userEmail);

    @Query("select distinct m from Movie m, InvoiceItem it, Invoice i where m.id = it.movie.id and i.user.email = :userEmail and i.id = it.invoice.id")
    Page<Movie> fetchMyMovies(@Param("userEmail") String userEmail, Pageable pageable);

}
