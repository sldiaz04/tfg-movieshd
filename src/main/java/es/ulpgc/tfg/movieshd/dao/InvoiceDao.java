package es.ulpgc.tfg.movieshd.dao;

import es.ulpgc.tfg.movieshd.entities.Invoice;
import es.ulpgc.tfg.movieshd.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceDao extends CrudRepository<Invoice, Long> {

    @Query("select i from Invoice i inner join fetch i.paymentInformation inner join fetch i.items it inner join fetch it.movie where i.id = ?1 and i.user = ?2")
    Invoice fetchInvoiceWithItems(Long id, User user);
}
