package es.ulpgc.tfg.movieshd.dao;

import es.ulpgc.tfg.movieshd.entities.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IMovieDao extends PagingAndSortingRepository<Movie, Long> {

    List<Movie> findByTitleLikeIgnoreCase(String term);

    Page<Movie> findByTitleLikeIgnoreCase(String term, Pageable pageable);

    List<Movie> findByLanguageIgnoreCase(String language);

    Page<Movie> findByLanguageIgnoreCase(String language, Pageable pageable);

    @Query("select m from Movie m inner join m.movieGenres mg inner join mg.genre g where g.name = ?1")
    List<Movie> findMoviesByGenre(String genre);

    @Query("select m from Movie m inner join m.movieGenres mg inner join mg.genre g where g.name = :genre")
    Page<Movie> findMoviesByGenre(@Param("genre") String genre, Pageable pageable);

    List<Movie> findFirst4ByOrderByTotalFavoritesDesc();

    List<Movie> findFirst10ByOrderByTotalFavoritesDesc();

    List<Movie> findByOrderByTotalFavoritesDesc();

    Page<Movie> findByOrderByTotalFavoritesDesc(Pageable pageable);

    List<Movie> findFirst10ByOrderByTotalPurchasesDesc();

    List<Movie> findByOrderByTotalPurchasesDesc();

    Page<Movie> findByOrderByTotalPurchasesDesc(Pageable pageable);

    List<Movie> findFirst10ByOrderByTotalViewsDesc();

    List<Movie> findByOrderByTotalViewsDesc();

    Page<Movie> findByOrderByTotalViewsDesc(Pageable pageable);

}
