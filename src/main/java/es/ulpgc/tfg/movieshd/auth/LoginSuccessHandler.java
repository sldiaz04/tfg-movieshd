package es.ulpgc.tfg.movieshd.auth;

import es.ulpgc.tfg.movieshd.utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.SessionFlashMapManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Component
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final SessionUtil sessionUtil;
    private MessageSource messageSource;
    private LocaleResolver localeResolver;

    @Autowired
    LoginSuccessHandler(SessionUtil sessionUtil, MessageSource messageSource, LocaleResolver localeResolver) {
        this.sessionUtil = sessionUtil;
        this.messageSource = messageSource;
        this.localeResolver = localeResolver;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
        FlashMap flashMap = new FlashMap();

        Locale locale = localeResolver.resolveLocale(request);
        flashMap.put("success", messageSource.getMessage("text.login.success", null, locale));
        flashMapManager.saveOutputFlashMap(flashMap, request, response);

        sessionUtil.loadPurchasedMoviesInSession(request.getSession(), authentication.getName());
        sessionUtil.loadFavoriteMoviesInSession(request.getSession(), authentication.getName());

        super.onAuthenticationSuccess(request, response, authentication);
    }


}
