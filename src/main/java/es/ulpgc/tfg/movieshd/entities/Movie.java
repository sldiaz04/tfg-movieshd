package es.ulpgc.tfg.movieshd.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String title;

    @NotEmpty
    @Column(name = "poster_image_url")
    private String posterImageURL;

    @NotEmpty
    @Column(name = "background_image_url")
    private String backgroundImageURL;

    @Column(name = "video_720p_url")
    private String video720pURL;

    @Column(name = "video_1080p_url")
    private String video1080pURL;

    @Column(name = "caption_en_url")
    private String captionEnUrl;

    @Column(name = "caption_fr_url")
    private String captionFrUrl;

    private String thumbnails;

    @NotEmpty
    private String description;

    @NotEmpty
    private String cast;

    private BigDecimal price;

    private Integer year;

    private Integer duration;

    @NotEmpty
    private String language;

    @NotEmpty
    private String quality;

    @NotEmpty
    private String trailer;

    @Column(name = "total_views")
    private Integer totalViews;

    @Column(name = "total_favorites")
    private Integer totalFavorites;

    @Column(name = "total_purchases")
    private Integer totalPurchases;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "movie_id")
    @JsonManagedReference
    private List<MovieGenre> movieGenres;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterImageURL() {
        return posterImageURL;
    }

    public void setPosterImageURL(String posterImageURL) {
        this.posterImageURL = posterImageURL;
    }

    public String getBackgroundImageURL() {
        return backgroundImageURL;
    }

    public void setBackgroundImageURL(String backgroundImageURL) {
        this.backgroundImageURL = backgroundImageURL;
    }

    public String getVideo720pURL() {
        return video720pURL;
    }

    public void setVideo720pURL(String video720pURL) {
        this.video720pURL = video720pURL;
    }

    public String getVideo1080pURL() {
        return video1080pURL;
    }

    public void setVideo1080pURL(String video1080pURL) {
        this.video1080pURL = video1080pURL;
    }

    public String getCaptionEnUrl() {
        return captionEnUrl;
    }

    public void setCaptionEnUrl(String captionEnUrl) {
        this.captionEnUrl = captionEnUrl;
    }

    public String getCaptionFrUrl() {
        return captionFrUrl;
    }

    public void setCaptionFrUrl(String captionFrUrl) {
        this.captionFrUrl = captionFrUrl;
    }

    public String getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(String thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Integer getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(Integer totalViews) {
        this.totalViews = totalViews;
    }

    public Integer getTotalFavorites() {
        return totalFavorites;
    }

    public void setTotalFavorites(Integer totalFavorites) {
        this.totalFavorites = totalFavorites;
    }

    public Integer getTotalPurchases() {
        return totalPurchases;
    }

    public void setTotalPurchases(Integer totalPurchases) {
        this.totalPurchases = totalPurchases;
    }

    public List<MovieGenre> getMovieGenres() {
        return movieGenres;
    }

    public void setMovieGenres(List<MovieGenre> movieGenres) {
        this.movieGenres = movieGenres;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Movie)) {
            return false;
        }
        Movie other = (Movie) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public int hashCode() {
        return 31 * id.hashCode() + title.hashCode();
    }
}
