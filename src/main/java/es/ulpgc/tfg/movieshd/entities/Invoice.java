package es.ulpgc.tfg.movieshd.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.*;

@Entity
@Table(name = "invoices")
public class Invoice implements Serializable {
    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;

    @Column(name = "create_at")
    private ZonedDateTime createAt;

    @Column(name = "price_without_taxes")
    private BigDecimal priceWithoutTaxes;

    @Column(name = "price_with_taxes")
    private BigDecimal priceWithTaxes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private User user;

    @OneToMany(mappedBy = "invoice", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<InvoiceItem> items;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "payment_information_id")
    private PaymentInformation paymentInformation;

    @PrePersist
    public void prePersist() {
        createAt = ZonedDateTime.now();
    }

    public Invoice() {
        items = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public ZonedDateTime getCreateAt() {
        return createAt;
    }

    public Calendar getCreateAtToCalendar() {
        return GregorianCalendar.from(createAt);
    }

    public void setCreateAt(ZonedDateTime createAt) {
        this.createAt = createAt;
    }

    public BigDecimal getPriceWithoutTaxes() {
        return priceWithoutTaxes;
    }

    public void setPriceWithoutTaxes(BigDecimal priceWithoutTaxes) {
        this.priceWithoutTaxes = priceWithoutTaxes;
    }

    public BigDecimal getPriceWithTaxes() {
        return priceWithTaxes;
    }

    public void setPriceWithTaxes(BigDecimal priceWithTaxes) {
        this.priceWithTaxes = priceWithTaxes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<InvoiceItem> getItems() {
        return items;
    }

    public void setItems(List<InvoiceItem> items) {
        this.items = items;
    }

    public void addItem(InvoiceItem invoiceItem) {
        items.add(invoiceItem);
    }

    public PaymentInformation getPaymentInformation() {
        return paymentInformation;
    }

    public void setPaymentInformation(PaymentInformation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }
}
