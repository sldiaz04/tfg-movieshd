package es.ulpgc.tfg.movieshd.entities;

public enum PaymentType {
    PAYPAL,
    CREDIT_CARD
}
