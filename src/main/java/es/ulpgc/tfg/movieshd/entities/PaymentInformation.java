package es.ulpgc.tfg.movieshd.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "payment_information")
public class PaymentInformation implements Serializable {
    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_type", length = 12)
    private PaymentType paymentType;

    // PayPal fields
    @Column(name = "payer_email")
    private String payerEmail;

    @Column(name = "payer_id")
    private String payerId;

    @Column(name = "payer_first_name")
    private String payerFirstName;

    @Column(name = "payer_last_name")
    private String payerLastName;

    @Column(name = "payment_id")
    private String paymentId;

    // Credit Card fields
    @Column(name = "card_type")
    private String cardType;

    @Column(name = "card_holder_name")
    private String cardHolderName;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "card_expiration")
    private String cardExpirationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public PaymentInformation setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public PaymentInformation setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
        return this;
    }

    public String getPayerId() {
        return payerId;
    }

    public PaymentInformation setPayerId(String payerId) {
        this.payerId = payerId;
        return this;
    }

    public String getPayerFirstName() {
        return payerFirstName;
    }

    public PaymentInformation setPayerFirstName(String payerFirstName) {
        this.payerFirstName = payerFirstName;
        return this;
    }

    public String getPayerLastName() {
        return payerLastName;
    }

    public PaymentInformation setPayerLastName(String payerLastName) {
        this.payerLastName = payerLastName;
        return this;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public PaymentInformation setPaymentId(String paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    public String getCardType() {
        return cardType;
    }

    public PaymentInformation setCardType(String cardType) {
        this.cardType = cardType;
        return this;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public PaymentInformation setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
        return this;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public PaymentInformation setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public String getCardExpiration() {
        return cardExpirationDate;
    }

    public PaymentInformation setCardExpiration(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
        return this;
    }

}
