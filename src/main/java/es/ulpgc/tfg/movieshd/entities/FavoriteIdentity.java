package es.ulpgc.tfg.movieshd.entities;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class FavoriteIdentity implements Serializable {
    @NotNull
    private Long movieId;

    @NotNull
    private String userEmail;

    public FavoriteIdentity(@NotNull Long movieId, @NotNull String userEmail) {
        this.movieId = movieId;
        this.userEmail = userEmail;
    }

    public FavoriteIdentity() {
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FavoriteIdentity that = (FavoriteIdentity) o;

        if (!movieId.equals(that.movieId)) return false;
        return userEmail.equals(that.userEmail);
    }

    @Override
    public int hashCode() {
        return 31 * movieId.hashCode() + userEmail.hashCode();
    }

}
