package es.ulpgc.tfg.movieshd.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "favorites")
public class Favorite {
    @EmbeddedId
    private FavoriteIdentity favoriteIdentity;

    public Favorite() {
    }

    public Favorite(FavoriteIdentity favoriteIdentity) {
        this.favoriteIdentity = favoriteIdentity;
    }

    public FavoriteIdentity getFavoriteIdentity() {
        return favoriteIdentity;
    }

    public void setFavoriteIdentity(FavoriteIdentity favoriteIdentity) {
        this.favoriteIdentity = favoriteIdentity;
    }
}
