package es.ulpgc.tfg.movieshd.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "invoice_items")
public class InvoiceItem implements Serializable {
    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer amount;

    @Column(name = "price_without_taxes")
    private BigDecimal priceWithoutTaxes;

    @Column(name = "price_with_taxes")
    private BigDecimal priceWithTaxes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Movie movie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Invoice invoice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getPriceWithoutTaxes() {
        return priceWithoutTaxes;
    }

    public void setPriceWithoutTaxes(BigDecimal priceWithoutTaxes) {
        this.priceWithoutTaxes = priceWithoutTaxes;
    }

    public BigDecimal getPriceWithTaxes() {
        return priceWithTaxes;
    }

    public void setPriceWithTaxes(BigDecimal priceWithTaxes) {
        this.priceWithTaxes = priceWithTaxes;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    BigDecimal calculateTotalAmount() {
        return new BigDecimal(amount).multiply(movie.getPrice());
    }
}
