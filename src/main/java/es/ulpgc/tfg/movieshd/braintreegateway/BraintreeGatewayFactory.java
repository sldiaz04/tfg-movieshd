package es.ulpgc.tfg.movieshd.braintreegateway;

import com.braintreegateway.BraintreeGateway;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;

import java.util.Map;
import java.util.Properties;

public class BraintreeGatewayFactory {
    private static final Log logger = LogFactory.getLog(BraintreeGatewayFactory.class);

    private BraintreeGatewayFactory() {
    }

    public static BraintreeGateway fromConfigMapping(Map<String, String> mapping) {
        return new BraintreeGateway(
                mapping.get("BT_ENVIRONMENT"),
                mapping.get("BT_MERCHANT_ID"),
                mapping.get("BT_PUBLIC_KEY"),
                mapping.get("BT_PRIVATE_KEY")
        );
    }

    public static BraintreeGateway fromConfigFile(Resource configFile) {
        Properties properties = new Properties();

        try {
            properties.load(configFile.getInputStream());
        } catch (Exception e) {
            logger.error("Exception: " + e);
        }

        return new BraintreeGateway(
                properties.getProperty("BT_ENVIRONMENT"),
                properties.getProperty("BT_MERCHANT_ID"),
                properties.getProperty("BT_PUBLIC_KEY"),
                properties.getProperty("BT_PRIVATE_KEY")
        );
    }
}
