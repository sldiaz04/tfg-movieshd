package es.ulpgc.tfg.movieshd.braintreegateway;

import com.braintreegateway.*;
import es.ulpgc.tfg.movieshd.MovieshdApplication;
import es.ulpgc.tfg.movieshd.email.MailClientBuilder;
import es.ulpgc.tfg.movieshd.email.model.Mail;
import es.ulpgc.tfg.movieshd.entities.*;
import es.ulpgc.tfg.movieshd.services.IMovieService;
import es.ulpgc.tfg.movieshd.services.IUserService;
import es.ulpgc.tfg.movieshd.services.InvoiceService;
import es.ulpgc.tfg.movieshd.utils.SessionUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

@Controller
@Secured("ROLE_ADMIN")
public class BraintreeGatewayController {

    private final Log logger = LogFactory.getLog(this.getClass());

    private static final String ERROR_FLASH_ATTRIBUTE = "error";

    private BraintreeGateway gateway = MovieshdApplication.getGateway();

    private final InvoiceService invoiceService;
    private final IUserService userService;
    private final IMovieService movieService;
    private final MailClientBuilder mailClientBuilder;
    private MessageSource messageSource;

    @Autowired
    public BraintreeGatewayController(InvoiceService invoiceService, IUserService userService,
                                      IMovieService movieService, MailClientBuilder mailClientBuilder, MessageSource messageSource) {
        this.invoiceService = invoiceService;
        this.userService = userService;
        this.movieService = movieService;
        this.mailClientBuilder = mailClientBuilder;
        this.messageSource = messageSource;
    }

    @GetMapping("/checkouts")
    public String checkout(Model model, RedirectAttributes redirectAttributes, HttpSession session, Locale locale) {
        List<Movie> movies = SessionUtil.getMoviesCartFromSession(session);

        if (movies == null || movies.isEmpty()) {
            redirectAttributes.addFlashAttribute("info", messageSource.getMessage("text.cart.is.empty", null, locale));
            return "redirect:/";
        }

        BigDecimal totalPrice = new BigDecimal(0);
        for (Movie movie : movies) {
            totalPrice = totalPrice.add(movie.getPrice());
        }

        model.addAttribute("clientToken", gateway.clientToken().generate());
        model.addAttribute("price", totalPrice);
        model.addAttribute("locale", locale);
        return "checkouts/new";
    }

    @PostMapping("/checkouts")
    public String paymentForm(@RequestParam(value = "price") String price, @RequestParam("payment_method_nonce") String nonce,
                              RedirectAttributes redirectAttributes, HttpSession session, Authentication auth, Locale locale) {
        BigDecimal totalPrice;
        String purchaseOrderNumber = RandomStringUtils.randomAlphanumeric(12);
        try {
            totalPrice = new BigDecimal(price);
        } catch (NumberFormatException e) {
            logger.error("Error: 81503: Amount is an invalid format.");
            redirectAttributes.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.cart.amount.invalid", null, locale));
            return "redirect:/cart/call";
        }

        User loggedUser = userService.findUserByEmail(auth.getName());
        if (loggedUser == null) {
            redirectAttributes.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, messageSource.getMessage("text.payment.user.not.found", null, locale));
            return "redirect:/cart/call";
        }

        TransactionRequest transactionRequest = new TransactionRequest()
                .amount(totalPrice)
                .paymentMethodNonce(nonce)
                .purchaseOrderNumber(purchaseOrderNumber)
                .taxAmount(totalPrice.subtract(getPriceWithoutTaxes(totalPrice)))
                .shippingAmount(new BigDecimal(0))
                .discountAmount(new BigDecimal(0))
                .shipsFromPostalCode("35010")
                .shippingAddress()
                .postalCode("35010")
                .countryCodeAlpha3("ESP")
                .done();

        List<Movie> moviesCartInSession = SessionUtil.getMoviesCartFromSession(session);
        moviesCartInSession.forEach(movie -> transactionRequest
                .lineItem()
                .name(movie.getTitle())
                .kind(TransactionLineItem.Kind.DEBIT)
                .quantity(new BigDecimal(1))
                .unitAmount(getPriceWithoutTaxes(movie.getPrice()))
                .totalAmount(movie.getPrice())
                .taxAmount(movie.getPrice().subtract(getPriceWithoutTaxes(movie.getPrice())))
                .discountAmount(new BigDecimal(0))
                .done());
        transactionRequest
                .options()
                .submitForSettlement(true)
                .done();

        Result<Transaction> result = gateway.transaction().sale(transactionRequest);

        Transaction transaction = result.getTarget();

        if (result.isSuccess() || result.getTransaction() != null) {
            List<Movie> statisticsMovies = new ArrayList<>();

            moviesCartInSession.forEach(movie -> {
                movie.setTotalPurchases(movie.getTotalPurchases() + 1);
                statisticsMovies.add(movie);
            });
            movieService.saveAll(statisticsMovies);

            PaymentInformation paymentInformation = new PaymentInformation();
            String methodOfPayment = transaction.getPaymentInstrumentType();

            if (methodOfPayment.equals(PaymentInstrumentType.PAYPAL_ACCOUNT)) {
                PayPalDetails payPalDetails = transaction.getPayPalDetails();
                paymentInformation.setPaymentType(PaymentType.PAYPAL)
                        .setPayerEmail(payPalDetails.getPayerEmail())
                        .setPayerId(payPalDetails.getPayerId())
                        .setPayerFirstName(payPalDetails.getPayerFirstName())
                        .setPayerLastName(payPalDetails.getPayerLastName())
                        .setPaymentId(payPalDetails.getPaymentId());
            } else if (methodOfPayment.equals(PaymentInstrumentType.CREDIT_CARD)) {
                CreditCard creditCard = transaction.getCreditCard();
                paymentInformation.setPaymentType(PaymentType.CREDIT_CARD);
                paymentInformation.setCardType(creditCard.getCardType())
                        .setCardHolderName(creditCard.getCardholderName())
                        .setCardNumber(creditCard.getMaskedNumber())
                        .setCardExpiration(creditCard.getExpirationDate());
            }

            Long invoiceId = createInvoice(purchaseOrderNumber, moviesCartInSession, loggedUser, paymentInformation, totalPrice);

            Mail mail = new Mail();
            mail.setTo(loggedUser.getEmail());
            mail.setSubject(messageSource.getMessage("text.payment.confirmation.email.subject", null, locale));
            mail.setEmailTemplate("emails/payment_confirmation");

            Map<String, Object> params = new HashMap<>();
            params.put("movies", moviesCartInSession);
            params.put("totalPrice", totalPrice);
            params.put("invoiceId", invoiceId);

            mailClientBuilder.prepareAndSend(mail, params, locale);

            List<Movie> moviesPurchasedInSession = SessionUtil.getMoviesPurchasedFromSession(session);
            if (moviesPurchasedInSession == null) {
                session.setAttribute("moviesPurchases", moviesCartInSession);
            } else {
                moviesPurchasedInSession.addAll(moviesCartInSession);
                session.setAttribute("moviesPurchases", moviesPurchasedInSession);
            }

            session.removeAttribute("movies");
            redirectAttributes.addFlashAttribute("success", messageSource.getMessage("text.payment.success", null, locale));
            return "redirect:/";
        } else {
            StringBuilder errorString = new StringBuilder();

            for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
                errorString.append("Error code: ").append(error.getCode()).append(" ");
                errorString.append("Error message: ").append(error.getMessage()).append("\n");
            }

            logger.error("Transaction error: " + errorString);
            redirectAttributes.addFlashAttribute(ERROR_FLASH_ATTRIBUTE, errorString);
            return "redirect:/cart/all";
        }
    }

    private BigDecimal getPriceWithoutTaxes(BigDecimal priceWithTaxes) {
        return priceWithTaxes.divide(new BigDecimal("1.07"), 2, BigDecimal.ROUND_HALF_UP);// 1.07 is 7% of taxes
    }

    private Long createInvoice(String purchaseOrderNumber, List<Movie> movies, User user,
                               PaymentInformation paymentInformation, BigDecimal priceWithTaxes) {
        Invoice invoice = new Invoice();
        invoice.setPurchaseOrderNumber(purchaseOrderNumber);
        invoice.setUser(user);
        invoice.setPaymentInformation(paymentInformation);
        invoice.setPriceWithTaxes(priceWithTaxes);

        BigDecimal invoicePriceWithoutTaxes = BigDecimal.ZERO;
        for (Movie movie : movies) {
            InvoiceItem invoiceItem = new InvoiceItem();
            invoiceItem.setInvoice(invoice);
            invoiceItem.setAmount(1);
            invoiceItem.setMovie(movie);
            invoiceItem.setPriceWithoutTaxes(getPriceWithoutTaxes(movie.getPrice()));
            invoiceItem.setPriceWithTaxes(movie.getPrice());
            invoice.addItem(invoiceItem);
            invoicePriceWithoutTaxes = invoicePriceWithoutTaxes.add(getPriceWithoutTaxes(movie.getPrice()));
        }
        invoice.setPriceWithoutTaxes(invoicePriceWithoutTaxes);
        invoiceService.saveInvoice(invoice);
        return invoice.getId();
    }
}
